# members.uwmcomputer.club

The live site can be found [here](https://members.uwmcomputer.club/)
or
[here](https://uwmcomputersociety.gitlab.io/website/members.uwmcomputer.club).

## Get started

### Install Harp

The site is generated using [Harp](http://harpjs.com/) — a static
website generator.

Install node.js ([downloads](https://nodejs.org/en/download/)), then
run `npm install -g harp`.

### Using Harp

TODO

## Create your own ~user directory

TODO

## Get your changes merged back in

TODO

